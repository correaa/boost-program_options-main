#if COMPILATION// -*-indent-tabs-mode:t;c-basic-offset:4;tab-width:4-*-
$CXX $0 -o $0x -lboost_program_options&&$0x $@&&rm $0x;exit
#endif
// © Alfredo A. Correa 2020

#ifndef BOOST_PROGRAM_OPTIONS_MAIN_HPP
#define BOOST_PROGRAM_OPTIONS_MAIN_HPP

#include<boost/program_options.hpp>

#include<cctype>
#include<fstream>
#include<iostream>

int main(int argc, char* argv[]);

namespace boost{
namespace program_options{

struct description{
	description(std::string title) : impl{title}{
		std::move(*this)
			("help,h", "display this help and exit")
			("environment", bool_switch()->default_value(true), "Accept options from environment")
		;
	}
	template<class T1, class... T2>
	description&& operator()(T1&& key, T2&&... value)&&{
		impl.add_options()(std::forward<T1>(key), std::forward<T2>(value)...);
		return std::move(*this);
	}
	template<class T1, class... T2>
	description&& positional(T1&& key, T2&&... value)&&{
		impl.add_options()(std::forward<T1>(key), std::forward<T2>(value)...);
		impl2.add(std::forward<T1>(key), -1);
		return std::move(*this);
	}
	template<class T1, class... T2>
	description&& positional(int max, T1&& key, T2&&... value)&&{
		impl.add_options()(std::forward<T1>(key), std::forward<T2>(value)...);
		impl2.add(std::forward<T1>(key), max);
		return std::move(*this);
	}
	template<class T1, class... T2>
	description&& hidden(T1&& key, T2&&... value)&&{
		hidden_.add_options()(std::forward<T1>(key), std::forward<T2>(value)...);
		return std::move(*this);
	}
private:
	program_options::options_description impl;
	program_options::positional_options_description impl2;
	program_options::options_description hidden_;
	friend int ::main(int argc, char* argv[]);
};

}

struct this_program{
	// this declaration forces the user to define boost::this_program::main
	static int main(boost::program_options::variables_map& vm);
	// this declaration forces the user to define boost::this_program::description
	static boost::program_options::description description;
};

namespace program_options{
	using variables = variables_map;
}
namespace program = program_options;

}

int main(int argc, char* argv[]){
	try{
		boost::program_options::variables_map vm;
		boost::program_options::command_line_parser parser{argc, argv};

		boost::this_program::description.impl.add_options()
			("config", boost::program_options::value<std::string>()->default_value(argv[0]+std::string(".ini")), "Configuration file")
		;

		boost::program_options::options_description all_options;
		all_options.add(boost::this_program::description.impl);
		all_options.add(boost::this_program::description.hidden_);

		parser
			.options(all_options)
			.positional(boost::this_program::description.impl2)
		//	.allow_unregistered()
		;
		boost::program_options::store(parser.run(), vm);
		if(vm.count("config")){
			std::ifstream ifs{vm["config"].as<std::string>().c_str()};
			if(ifs) store(parse_config_file(ifs, boost::this_program::description.impl), vm);
		}
		if(vm.count("environment")){
			store(parse_environment(
				boost::this_program::description.impl,
				[](std::string const& i_env_var){return i_env_var=="HOSTNAME"?"hostname":"";}
			), vm);
		}
		boost::program_options::notify(vm);
		if(vm.count("help") ){
			std::cout<<"Usage: "<< argv[0];
			std::string last = "";
			int rep = 0;
			for(auto i = 0u; i < boost::this_program::description.impl2.max_total_count(); i++){
				std::string const&n = boost::this_program::description.impl2.name_for_position(i);
				if(n == last){
					if(!rep) std::cout<<" ...";
					if(rep++ > 1000) break;
				}else{
					std::cout<<" "<< n;
					last = n;
					rep = 0;
				}
			}
			std::cout<<" [OPTIONS]...  \n"<< boost::this_program::description.impl <<'\n';
			return 0;
		}
		boost::this_program::main(vm);
	}catch(boost::program_options::error const& ex){
		std::cerr<< argv[0] <<": "<< ex.what() <<"\nTry '"<< argv[0] <<" --help' for more information\n";
		return 1;
	}
}
#endif

#if not __INCLUDE_LEVEL__ // _TEST_BOOST_PROGRAM_OPTIONS_MAIN

namespace program = boost::program; 
using boost::this_program;

program::description this_program::description = program::description
	{"Description of a Test program to show OPTIONS"}
	("pi"   , program::value<float>()->default_value(3.14f), "Pi" )
	("age"  , program::value<int>()                        , "Age")
	("phone", program::value<std::vector<std::string>>()->multitoken()->zero_tokens()->composing(), "Phone")
	.hidden("optimization", program::value<bool>()->default_value(false)->implicit_value(true), "Optimization (hidden)")
	.positional(-1, "files", program::value<std::vector<std::string>>()->multitoken()->zero_tokens()->composing(), "File")
;

int this_program::main(program::variables& argm){
	using std::cout;

	if(argm.count("age"  )) cout<<"Age: "<< argm["age"].as<int  >() <<'\n';
	if(argm.count("pi"   )) cout<<"Pi:  "<< argm["pi" ].as<float>() <<'\n';
	if(argm.count("optimization")) cout<<"optimization:  "<< argm["optimization"].as<bool>() <<'\n';
	if(argm.count("phone"))
		for(auto e: argm["phone"].as<std::vector<std::string>>()) cout<<"phone "<< e <<'\n';

	if(argm.count("files"))
		for(auto e: argm["files"].as<std::vector<std::string>>()) cout<<"files "<< e <<'\n';

	return 0;
}

#endif

