#if COMPILATION// -*-indent-tabs-mode:t;c-basic-offset:4;tab-width:4-*-
$CXX $0 -o $0x -lboost_program_options&&$0x $@&&rm $0x;exit
#endif
// © Alfredo A. Correa 2020

#include "../main.hpp"

namespace program = boost::program; 
using boost::this_program;

program::description this_program::description = program::description
	{"Description of example program"}
		("compression,c", program::value<int>()->default_value(5)->implicit_value(10),"compression level")
		.positional("input-files", program::value<std::vector<std::string>>(), "input files")
		("version,v", "display the version number")
;

int this_program::main(program::variables& argm){
	using std::cout;

	if(argm.count("version")){
		std::cout << "version 2.0" << std::endl;
		return 0;
	}

	if(argm.count("compression")) 
		std::cout<<"compression level "<< argm["compression"].as<int>() <<'\n';

	if(argm.count("input-files"))
		for(std::string file : argm["input-files"].as<std::vector<std::string>>())
			std::cout << "Input file " << file << std::endl;

	return 0;
}

