#if COMPILATION// -*-indent-tabs-mode:t;c-basic-offset:4;tab-width:4-*-
$CXX $0 -o $0x -lboost_program_options&&$0x $@&&rm $0x;exit
#endif
// © Alfredo A. Correa 2020

#include "../main.hpp"

namespace program = boost::program; 
using boost::this_program;

program::description this_program::description = program::description
	{"Description of a Test program to show OPTIONS"}
	("pi"   , program::value<float>()->default_value(3.14f)                                                      , "Pi"                   )
	("age"  , program::value<int>()->implicit_value(33)                                                          , "Age"                  )
	("address", program::value<std::string>()->default_value("unknown")->implicit_value("here")                  , "Address"              )
	("phone", program::value<std::vector<std::string>>()->multitoken()->zero_tokens()->composing()               , "Phone"                )
	.hidden("optimization", program::value<bool>()->default_value(false)->implicit_value(true)                   , "Optimization (hidden)")
	.positional(-1, "files", program::value<std::vector<std::string>>()->multitoken()->zero_tokens()->composing(), "File"                 )
;

int this_program::main(program::variables& argm){
	using std::cout;

	if(argm.count("age"    )) cout<<"Age    : "<< argm["age"].as<int  >() <<'\n';
	if(argm.count("address")) cout<<"Address: "<< argm["address" ].as<std::string>() <<'\n';
	if(argm.count("pi"     )) cout<<"Pi     : "<< argm["pi" ].as<float>() <<'\n';
	if(argm.count("optimization")) cout<<"optimization:  "<< argm["optimization"].as<bool>() <<'\n';
	if(argm.count("phone"))
		for(auto e: argm["phone"].as<std::vector<std::string>>()) cout<<"phone "<< e <<'\n';

	if(argm.count("files"))
		for(auto e: argm["files"].as<std::vector<std::string>>()) cout<<"files "<< e <<'\n';

	return 0;
}

