<!--
(pandoc `#--from gfm` --to html --standalone --metadata title=" " $0 > $0.html) && firefox --new-window $0.html; sleep 5; rm $0.html; exit
-->

# Boost.ProgramOptions.Main

(not an official Boost library)

_© Alfredo A. Correa, 2020_

B.ProgramOptions.Main is a header that simplifies the use of Boost.ProgramOptions.

Using this library, a typical program is specified in two sections. 
First the options specification using the features of the Boost.ProgramOptions library.

```c++
// example/example1.cpp
#include "boost-program_options-main/main.hpp" // include this header library

namespace program = boost::program; 
using boost::this_program;

program::description this_program::description = program::description
	{"Description of a Test program to show OPTIONS"}
	("pi"   , program::value<float>()->default_value(3.14f), "Pi" )
	("age"  , program::value<int>()                        , "Age")
	("phone", program::value<std::vector<std::string>>()->multitoken()->zero_tokens()->composing(), "Phone")
	.hidden("optimization", program::value<bool>()->default_value(false)->implicit_value(true), "Optimization (hidden)")
	.positional(-1, "files", program::value<std::vector<std::string>>()->multitoken()->zero_tokens()->composing(), "File")
;
...
```

The overall description is passed as first the first line in curly brackets (constructor).
The normal options as passed as lines in parenthesis `("long_name,shortcut", SPECIFICATION, "Human readable description")`.

Some options can be hidden (not shown in the automatically generated help message), see `.hidden(...)` above.
Some options are also positional (they need no label prefix, e.g. "--files"), see `.positional(N, "long_name,shortcut", SPECIFICATION, "Human readable descrption")`.
`N` is the maximum number of positional arguments allowed, `-1` or omitted means no limit.

Second, a substitute of the `main` function that takes Boost.ProgramOptions variables directly.

```c++
...
int this_program::main(program::variables& argm){
	using std::cout;

	if(argm.count("age"  )) cout<<"Age: "<< argm["age"].as<int  >() <<'\n';
	if(argm.count("pi"   )) cout<<"Pi:  "<< argm["pi" ].as<float>() <<'\n';
	if(argm.count("optimization")) cout<<"optimization:  "<< argm["optimization"].as<bool>() <<'\n';
	if(argm.count("phone"))
		for(auto e: argm["phone"].as<std::vector<std::string>>()) cout<<"phone "<< e <<'\n';

	if(argm.count("files"))
		for(auto e: argm["files"].as<std::vector<std::string>>()) cout<<"files "<< e <<'\n';

	return 0;
}
```
Except for the signature function, this is the normal use of Boost.ProgramOptions variables (`variable_map`).

There no need to define a `int main()` or `int main(int argc, char* argv[])` function or deal with raw command line parameters (`argc`, `argv`).
`main` is defined in the header.

NOTE: it is an error to define a `main` function when including `boost_program_options-main/main.hpp`. `argc` and `argv` are not accessible from `this_program::main`.

The program, `cpp` file containing the `this_program::description` section and `this_program::main`, is compiled normally and linked to `boost_program_options`. 
Based on the program description, a help message and other options are generated automatically.

This is the typical output of `example/example2.cpp`.

```bash
$ c++ example2.cpp -lboost_program_options -o example2
$ ./example2.x --help
Usage: ./example2.cppx input-files ... [OPTIONS]...  
Description of example program options:
  -h [ --help ]                         display this help and exit
  --environment                         accept options from environment
  -c [ --compression ] [=arg(=10)] (=5) compression level
  --input-files arg                     input files
  -v [ --version ]                      display the version number
  --config arg (=./example2.cppx.ini)   configuration file
```

For more information on Boost.ProgramOptions and how to specify each option in detail, see https://www.boost.org/doc/libs/1_72_0/doc/html/program_options.html 

